Step(Recommend copy and paste line by line)

For master node

[Ubuntu]
1. Do [1-install-master.sh](https://gitlab.com/theAutotelicX/kube-installation/blob/master/1-kubeadm/1-install-master.sh)

[CentOS]
1. Do [1-install-master-centos.sh](https://gitlab.com/theAutotelicX/kube-installation/blob/master/1-kubeadm/1-kubeadm-master-centos.sh)

For slave node

[Ubuntu]
1. Do [2-install-slave.sh](https://gitlab.com/theAutotelicX/kube-installation/blob/master/1-kubeadm/2-install-slave.sh)

[CentOS]
1. Do [2-install-slave-centos.sh](https://gitlab.com/theAutotelicX/kube-installation/blob/master/1-kubeadm/2-kubeadm-slave-centos.sh)
2. Execute kubeadm join which get from master node i.e.  kubeadm join --token xxxx 172.31.49.128:6443 --discovery-token-ca-cert-hash sha256:xxxx

For master node
Test if it work by run [3-test-install.sh](https://gitlab.com/theAutotelicX/kube-installation/blob/master/1-kubeadm/3-test-install.sh)

All pod should be in working state.

Optional install NFS to all nodes. So, each node can connect together.(For OSCCP, this process is required)

For master node

[Ubuntu]
1. Do [4-Add-NFS-Master.sh](https://gitlab.com/theAutotelicX/kube-installation/blob/master/1-kubeadm/4-Add-NFS-Master.sh)

[CentOS]
1. Do [4-Add-NFS-Master-centos.sh](https://gitlab.com/theAutotelicX/kube-installation/blob/master/1-kubeadm/4-Add-NFS-Master-centos.sh)

For slave node

[Ubuntu]
1. Do [5-Add-NFS-Slave.sh](https://gitlab.com/theAutotelicX/kube-installation/blob/master/1-kubeadm/5-Add-NFS-Slave.sh)

[CentOS]
1. Do [5-Add-NFS-Slave-centos.sh](https://gitlab.com/theAutotelicX/kube-installation/blob/master/1-kubeadm/5-Add-NFS-Slave-centos.sh)

Note: 
If `kubectl get pods -n kube-system`
>  weavenet 1/2 error  

try,

`kubectl logs -n kube-system <weave pod> weave` #<- if address issue
try:  
`kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')&env.IPALLOC_RANGE=10.32.0.0/16"`

If `kubectl get nodes`, get The connection to the server xxx.xxx.xxx.xxx:6443 was refused - did you specify the right host or port?

try:

`sudo swapoff -a`

`sudo systemctl restart kubelet`

`sudo systemctl restart kubelet.service`

If unable to `kubeadm init`, try `sudo service docker restart` then delete kill docker container, remove docker container and delete docker images

If core-dns is not working, try https://stackoverflow.com/questions/54466359/coredns-crashloopbackoff-in-kubernetes or some dirty solution such as  

`kubectl edit cm coredns -n kube-system` delete loop and delete coredns pod 

For an easy way to follow use this link: https://www.cloudtechnologyexperts.com/kubeadm-on-aws/


