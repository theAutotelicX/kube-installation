## To Update pod image

### Using `kubectl delete pod pod_name`

or

![](Diagram/UpdatePodImage.PNG )

### "myimage" of python pod is "r15axzc/loan-python"

Source: [https://stackoverflow.com/questions/40366192/kubernetes-how-to-make-deployment-to-update-image](https://stackoverflow.com/questions/40366192/kubernetes-how-to-make-deployment-to-update-image)
