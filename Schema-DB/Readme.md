Create mysql database on ubuntu

Install mysql-server

`sudo apt-get install mysql-server`

`systemctl start mysql`

`sudo mysql -u root -p`

`CREATE USER '(User)'@'%' IDENTIFIED BY '(Password)';`

`GRANT ALL PRIVILEGES ON *.* TO 'User'@'%';`

Install mysql workbench

`sudo apt install mysql-workbench`

Edit file to make it visible to mysql-workbench

`sudo nano /etc/mysql/mysql.conf.d/mysqld.cnf`

change from 127.0.0.1 to 0.0.0.0

https://medium.com/artisan-digital-agency/%E0%B8%A7%E0%B8%B4%E0%B8%98%E0%B8%B5%E0%B9%80%E0%B8%8A%E0%B8%B7%E0%B9%88%E0%B8%AD%E0%B8%A1%E0%B8%95%E0%B9%88%E0%B8%AD-mysql-database-%E0%B8%9A%E0%B8%99-ubuntu-server-%E0%B8%94%E0%B9%89%E0%B8%A7%E0%B8%A2-mysqlworkbench-1d48a29be812

--------------------------------------------------------------------
CentOS7

#begin with sudo

`yum localinstall https://dev.mysql.com/get/mysql80-community-release-el7-3.noarch.rpm`

`yum install mysql-community-server`

`systemctl enable mysqld`

`systemctl start mysqld`

`systemctl status mysqld`

#grep temporary password

`grep 'temporary password' /var/log/mysqld.log`

`mysql -u root -p`

#create user

`ALTER USER 'root'@'localhost' identified by 'password';`

`ALTER USER 'root'@'localhost' PASSWORD EXPIRE NEVER;`

`CREATE USER 'root'@'%' IDENTIFIED BY 'password';`

`GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' WITH GRANT OPTION;`

exit from MySQL

`nano /etc/my.cnf`

add 'bind-address = 0.0.0.0'

`service mysqld restart`

#SET GLOBAL validate_password.policy=LOW; # to make password only count >= 8 chars
#SET GLOBAL validate_password.length=6;
#SET GLOBAL validate_password.number_count = 0;
#SSL/TLS is required
https://www.digitalocean.com/community/tutorials/how-to-configure-ssl-tls-for-mysql-on-ubuntu-18-04
