## OSCCP

![](Diagram/osccp-logo.png)
#### Requirement
*  Ubuntu 18.04LTS (or CentOS7 - Experimental)
*  At least 2 cores and 2GB of RAM
*  MySQL-Server
*  MySQL-Workbench that can connect with MySQL-Server
*  Dockerhub account
*  Full network connectivity between all machines in the cluster (public or private network is fine)
*  Swap are disabled(with swapoff -a)

### Ports require to open

![](Diagram/port-req.PNG)
* 3306 for MySQL

Following ports for NFS

* UDP: 111, 1039, 1047, 1048 and 2049.
* TCP: 111, 1039, 1047, 1048 and 2049.

Following ports for OSCCP services

* 30830 - kubernetes dashboard
* 31200 - VueUI
* 32222 - Flower-CeleryMonitor
* 32333 - Spark Master
* 32000 - Python-Flask
* 32111 - Spark Driver


#### List of component install on EC2
*  Kubernetes with kubeadm 1.14
*  Kubernetes dashboard
*  Elasticsearch 6.5.2
*  Python v.3.6
*  Spark 2.4
*  Vue

List of python dependencies
*  Flask
*  Flask-Restplus
*  Flask-Cors
*  MySQL.Connector
*  Celery
*  Psycopg2
*  Pandas
*  Elasticsearch
*  PyMongo
*  PySpark

-----------
### OSCCP Architecture
![](Diagram/Stateless_Architecture.png)

### OSCCP NFS Architecture and What node allow to access services(below NFS)
![](Diagram/Python-EFS.png)

### In addition, this repository provides how to install kubernetes and deploy microservice inside
* For deploy kubernetes, follow [1-kubeadam](https://gitlab.com/theAutotelicX/kube-installation/tree/master/1-kubeadm)
* For deploy kubernetes dashboard, follow [1.5-install-kubernetes-dashboard](https://gitlab.com/theAutotelicX/kube-installation/tree/master/1.5-kubernetes_dashboard)
