#!/usr/bin/env python3
import OSCCP as osccp
import pandas as pd
import sys
from elasticsearch import helpers
osccp = osccp.OSCCP(sys.argv[1])

f = osccp.readFile('fakedataset099S.csv')

df = pd.read_csv(f)

df1 = df.groupby(['col1']).size().reset_index(name='count')

#load to elasticsearch

df1.to_csv('group1.csv',encoding='utf-8')

indexName = osccp.loadFileToElastic(osccp.getReadPath('group1.csv'),'fakedata')

# group size col2

df2 = df.groupby(['col2']).size().reset_index(name='count')

#put to elasticsearch

es = osccp.getElasticsearch()

document = df2.to_dict(orient='records')

helpers.bulk(es,document,index=indexName,doc_type=osccp.getDocType())

#writeFile indexName; so i will not forget

content = 'indexName: '+ indexName
osccp.writeFile('myindex.txt',content)
osccp.addEndpoint('myindex.txt')

#append df and set endpoint

df1 = df1.append(df2)

df1.to_csv(osccp.getWritePath('groupComplete.csv'),encoding='utf-8')

osccp.addEndpoint('groupComplete.csv')