CREATE SCHEMA el;
use el;

CREATE TABLE `user` (
  `userID` int(4) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `firstName` varchar(45) NOT NULL,
  `lastName` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `permission` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`userID`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

CREATE TABLE `access` (
  `accessID` int(4) NOT NULL AUTO_INCREMENT,
  `accessName` varchar(150) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `dbms` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `ipAddress` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `dbUsername` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `dbPassword` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `dbName` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `userID` int(4) NOT NULL,
  `port` int(10) NOT NULL,
  PRIMARY KEY (`accessID`),
  KEY `access_user_idx` (`userID`),
  CONSTRAINT `access_user` FOREIGN KEY (`userID`) REFERENCES `user` (`userID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

CREATE TABLE `elasticindex` (
  `indexID` int(11) NOT NULL AUTO_INCREMENT,
  `indexName` varchar(10) COLLATE utf8_bin NOT NULL,
  `alias` varchar(200) COLLATE utf8_bin NOT NULL,
  `userID` int(11) NOT NULL,
  PRIMARY KEY (`indexID`),
  KEY `index_user_idx` (`userID`),
  CONSTRAINT `user_index` FOREIGN KEY (`userID`) REFERENCES `user` (`userID`)
) ENGINE=InnoDB AUTO_INCREMENT=179 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

CREATE TABLE `request` (
  `requestID` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `submittedDate` datetime NOT NULL,
  `finishedDate` datetime DEFAULT NULL,
  `result` json DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `inputType` int(1) NOT NULL,
  `source` varchar(45) DEFAULT NULL,
  `accessID` int(4) DEFAULT NULL,
  `userID` int(4) NOT NULL,
  PRIMARY KEY (`requestID`),
  KEY `request_user_idx` (`userID`),
  KEY `request_access_idx` (`accessID`),
  CONSTRAINT `request_access` FOREIGN KEY (`accessID`) REFERENCES `access` (`accessID`) ON DELETE SET NULL,
  CONSTRAINT `request_user` FOREIGN KEY (`userID`) REFERENCES `user` (`userID`)
) ENGINE=InnoDB AUTO_INCREMENT=390 DEFAULT CHARSET=utf8;
