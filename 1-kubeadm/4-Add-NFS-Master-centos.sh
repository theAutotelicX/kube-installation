yum install nfs-utils
mkdir -p /nfsdisk/efs

mkdir -p /nfsdisk/efs/app/upload
mkdir -p /nfsdisk/efs/app/output
mkdir -p /nfsdisk/efs/elasticsearch
mkdir -p /nfsdisk/efs/storage

chmod -R 777 /nfsdisk/efs
systemctl enable nfs-server.service
systemctl start nfs-server.service
nano /etc/exports

cat <<EOF > /etc/exports
/nfsdisk/efs  192.168.217.130(rw,async,insecure,no_subtree_check,nohide)
/nfsdisk/efs  10.34.2.7(rw,async,insecure,no_subtree_check,nohide)
/nfsdisk/efs  10.34.2.38(rw,async,insecure,no_subtree_check,nohide)
EOF
exportfs -a
systemctl restart nfs-server

#guide
#https://www.howtoforge.com/tutorial/setting-up-an-nfs-server-and-client-on-centos-7/
#http://thaiopensource.org/%E0%B8%A1%E0%B8%B2%E0%B9%83%E0%B8%8A%E0%B9%89%E0%B8%87%E0%B8%B2%E0%B8%99-nfs-%E0%B8%9A%E0%B8%99-centos-7-%E0%B8%81%E0%B8%B1%E0%B8%99/