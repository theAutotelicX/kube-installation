// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import 'core-js/es6/promise'
import 'core-js/es6/string'
import 'core-js/es7/array'
// import cssVars from 'css-vars-ponyfill'
import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import App from './App'
import router from './router'
import axios from'axios'

// todo
// cssVars()
Vue.use(BootstrapVue)
//Vue.use(axios)
Vue.prototype.$axios = axios
//Vue.prototype.$globals = globalStore
Vue.mixin({
	data:() => {
		return {
			DBoptions: [
		        { text: 'MySQL', value: '1'},
		        { text: 'PostgreSQL', value: '2'},
		        { text: 'MongoDB', value: '3'},
		        { text: 'Oracle', value: '4'}
	      	],
	      	//BaseURL: 'http://192.168.99.100:31308'
			BaseURL: 'http://10.34.2.38:32000'
			//BaseURL: 'http://localhost:5000'
		}
	},
	methods: {
	    getBadgeSource (inPutType) {
	      return inPutType === 0 ? 'success'
	      		: inPutType === 1 ? 'danger' : 'secondary'
	    },
	    getDataSource (inPutType) {
	      return inPutType === 0 ? 'DB'
	      		: inPutType === 1 ? 'File' : 'Task'
	    },
		isFinished(finishedDate) {
		    if(finishedDate==null)
		    {
		      return 'Not finished'
		    }
		    else
		    {
		      return finishedDate
		    }
		},
		getStatusBadge (status) {
	      return status === 0 ? 'success'
		          : status === 1 ? 'primary' : 'danger'
    	},
	    getStatus (status) {
	      return status === 0 ? 'Succeeded'
	            : status === 1 ? 'Pending' : 'Failed'
	    },
	    getSysErrorMsg() {
	    	return ', Please Contact System Administrator'
	    },
	    getDBName(dbms) {
	      for (var i = 0; i < this.DBoptions.length; i++) {
	        if(this.DBoptions[i].value == dbms) {
	          return this.DBoptions[i].text
	        }
	      }
	    }
	}
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: {
    App
  }
})
