apt-get install nfs-kernel-server
mkdir -p /nfsdisk/efs

mkdir -p /nfsdisk/efs/app/upload
mkdir -p /nfsdisk/efs/app/output
mkdir -p /nfsdisk/efs/elasticsearch
mkdir -p /nfsdisk/efs/storage

chmod -R 777 /nfsdisk/efs
cp -a /etc/exports /etc/exports.backup
#check ip range of system
echo '/nfsdisk/efs  10.34.2.81(rw,async,insecure,no_subtree_check,nohide)' | sudo tee -a /etc/exports
echo '/nfsdisk/efs  10.34.2.7(rw,async,insecure,no_subtree_check,nohide)' | sudo tee -a /etc/exports
echo '/nfsdisk/efs  10.34.2.38(rw,async,insecure,no_subtree_check,nohide)' | sudo tee -a /etc/exports
#for range ip
#echo '/efs  10.34.2.0/24(rw,async,insecure,no_subtree_check,nohide)' | sudo tee -a /etc/exports
exportfs -a
systemctl restart nfs-kernel-server

#guide
#https://opensource.cc.psu.ac.th/%E0%B8%95%E0%B8%B4%E0%B8%94%E0%B8%95%E0%B8%B1%E0%B9%89%E0%B8%87_nfs_server_%E0%B8%9A%E0%B8%99_ubuntu_16.04_server
#https://vitux.com/install-nfs-server-and-client-on-ubuntu/