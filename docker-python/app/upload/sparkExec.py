from pyspark import sql,SparkConf,SparkContext
from pyspark.sql import SQLContext,SparkSession
sparkConf = SparkConf()
sparkConf.setMaster('spark://spark-maste.default.svc.cluster.local:7077')
sparkConf.setAppName('spark')
sparkConf.set("spark.cores.max", "1")
sparkConf.set("spark.executor.cores","1")
sparkConf.set("spark.executor.instances","2")
sparkConf.set("spark.executor.memory", "512m")
sparkConf.set("spark.task.cpus", "1")
sparkConf.set("spark.driver.host", "10.104.159.104")
sparkConf.set("spark.driver.port", "8888")
sparkConf.set("spark.driver.bindAddress", "0.0.0.0")
sparkConf.set("spark.driver.blockManager.port", "8888")
sc = SparkContext(conf=sparkConf)
spark = SparkSession.builder.config(conf=sparkConf).getOrCreate()

print('deassx')

words = 'the quick brown fox jumps over the\
        lazy dog the quick brown fox jumps over the lazy dog'
seq = words.split()
data = sc.parallelize(seq)
counts = data.map(lambda word: (word, 1)).reduceByKey(lambda a, b: a + b).collect()
print(dict(counts))
sc.stop()