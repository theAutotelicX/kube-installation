###Assume put file in efs/storage/ already

import OSCCP as osccp
from pyspark import sql,SparkConf,SparkContext
from pyspark.sql import SQLContext,SparkSession
import sys

osccp = osccp.OSCCP(sys.argv[1])

sc = osccp.getSparkContext('group')

sql_c = SQLContext(sc)

df = sql_c.read.csv('/efs/storage/checkouts-by-title.csv') #your file here

df.show(n=2)
