export default {
  items: [
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'icon-speedometer'
    },
    {
      name: 'My Request',
      url: '/my-request',
      icon: 'fa fa-book'
    },
    {
      name: 'My Access',
      url: '/my-access',
      icon: 'fa fa-link'
    },
    {
      name: 'My Elasticsearch Index',
      url: '/my-index',
      icon: 'fa fa-link'
    },
      {
      name: 'Submit Request',
      url: '/submit-request',
      icon: 'fa fa-plus-circle'
    },
      {
      name: 'Setting',
      url: '/setting',
      icon: 'fa fa-cog'
    }
  ]
}
