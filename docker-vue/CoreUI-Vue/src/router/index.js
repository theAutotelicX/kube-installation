import Vue from 'vue'
import Router from 'vue-router'
import {globalStore} from '../main.js'
// Containers
const DefaultContainer = () => import('@/containers/DefaultContainer')

// Views
const Dashboard = () => import('@/views/Dashboard')
const MyRequest = () => import('@/views/MyRequest')
const SubmitRequest = () => import('@/views/SubmitRequest')
const Detail = () => import('@/views/Detail')
const MyAccess = () => import('@/views/MyAccess')
const MyIndex = () => import('@/views/MyIndex')

// User and Authentication
const Login = () => import('@/views/Login')
const Setting = () => import('@/views/UserSetting')
const Register = () => import('@/views/Register')

Vue.use(Router)

const router = new Router({
  mode: 'history', // https://router.vuejs.org/api/#mode
  linkActiveClass: 'open active',
  scrollBehavior: () => ({ y: 0 }),
  routes: [
    {
      path: '/',
      redirect: '/dashboard',
      name: 'Home',
      component: DefaultContainer,
      children: [
        {
          path: 'dashboard',
          name: 'Dashboard',
          component: Dashboard,
          meta:
          {
            requiresAuth: true
          }
        },
        {
          path: 'my-request',
          //name: 'MyRequest',
          meta: { label: 'My Request'},
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: '',
              component: MyRequest,
              meta:
              {
                requiresAuth: true
              }
            },
            {
              path: ':reqid',
              name: 'Request Detail',
              component: Detail,
              meta:
              {
                label: 'Request Detail',
                requiresAuth: true
              },
            },
          ]
        },
        {
          path: 'my-access',
          //name: 'MyAccess',
          component: MyAccess,
          meta:
            {
              label: 'My Access List',
              requiresAuth: true
            }
        },
        {
          path: 'my-index',
          //name: 'MyAccess',
          component: MyIndex,
          meta:
            {
              label: 'My Elasticsearch Index',
              requiresAuth: true
            }
        },
        {
          path: 'submit-request',
          name: 'SubmitRequest',
          component: SubmitRequest,
          meta:
            {
              label: 'Submit a new request',
              requiresAuth: true
            }
        },
        {
          path: 'setting',
          name: 'Setting',
          component: Setting,
          meta:
            {
              label: 'User Setting',
              requiresAuth: true
            }
        }
      ]
    },
    {
      path: '/pages',
      redirect: '/pages/404',
      name: 'Pages',
      component: {
        render (c) { return c('router-view') }
      },
      children: [
        {
          path: 'login',
          name: 'Login',
          component: Login,
          meta:
            {
              requiresAuth: false
            }
        },
        {
          path: 'register',
          name: 'Register',
          component: Register,
          meta:
            {
              requiresAuth: false
            }
        }
      ]
    },
    {
      path: '*',
      redirect: '/',
      meta:
      {
        requiresAuth: true
      }
    }
]
})
router.beforeEach((to, from, next) => {
  if(to.matched.some(record => record.meta.requiresAuth)) {
          if (localStorage.getItem('token') == null) {
              next('pages/login')
          } 
          else {
              next()
          }
    }
  else if(to.name == 'Login' && localStorage.getItem('token'))
  {
      next('/')
  }
      next()
})
export default router