#!/usr/bin/env python3
import json
import mysql.connector 
import psycopg2
import pandas as pd
import csv
import os
from elasticsearch import Elasticsearch, helpers
from pymongo import MongoClient
import cx_Oracle
import datetime
import jwt
import random
import string
import smtplib
import requests
import uuid
from pyspark import sql,SparkConf,SparkContext
from pyspark.sql import SQLContext,SparkSession
import configparser
import folium
import geopandas
from branca.colormap import linear
import sqlite3
import json

os.environ['SPARK_HOME']='/spark-2.4.3-bin-hadoop2.7'
os.environ['PYSPARK_PYTHON']="/usr/bin/python3"
os.environ['PYSPARK_DRIVER_PYTHON']="/usr/bin/python3"
os.environ['JAVA_HOME']='/usr/lib/jvm/java-1.8.0-openjdk-amd64'

#public variable
#elasticsearchurl = '192.168.99.100' #local
elasticsearchurl = 'elasticsearch' #kubernetes
elasticsearchport = '9200'

UPLOAD_FOLDER = '/efs/app/upload/'
OUTPUT_FOLDER = '/efs/app/output/'
SECRET_CA = '/app/secret/ca-cert.pem'

#_configDB
configINI = configparser.ConfigParser()
configINI.read('/app/config.ini')
_config = {
        'user' : configINI['MYSQL']['user'],
        'password': configINI['MYSQL']['password'],
        'host' : configINI['MYSQL']['host'],
        'database' : configINI['MYSQL']['database'],
        'ssl_ca' : SECRET_CA
    }

docType='doc'

class OSCCP:
    def __init__(self, reqID, userID='', dbms = 0, cnx=None, cur=None):
        '''reqID is argv[1]'''
        self.reqID = reqID
        self.userID = userID
        self.dbms = dbms
        self.cnx = cnx
        self.cur = cur
        self.outputList = list()
    def getUserID(self):
        cnx = mysql.connector.connect(**_config)
        cur = cnx.cursor()
        cur.execute("SELECT userID FROM el.request WHERE requestID = %s", (self.reqID,))
        result = cur.fetchone()[0]
        cur.close()
        cnx.close()
        return result
    def getDbConnection(self):
        '''use after parseInfo()'''
        try:
            cnx = mysql.connector.connect(**_config)
            cur = cnx.cursor()
            cur.execute("SELECT dbms, ipAddress, dbUsername, dbPassword, port, dbName FROM el.request INNER JOIN access \
            ON request.accessID = access.accessID WHERE requestID = %s", (str(self.reqID),))
            result = cur.fetchone()
        except mysql.connector.Error as err:
            print(err)
            return err
        result = self.byteArraytoString(result)
        dbms = int(result[0])
        ipAddress = result[1]
        dbPort = int(result[4])
        dbUsername = result[2]
        dbPassword = result[3]
        dbName = result[5]
        cur.close()
        cnx.close()
        ## MySQL Connection
        if dbms == 1:
            try:
                self.cnx = mysql.connector.connect(host=ipAddress, port=dbPort, user=dbUsername, password=dbPassword, database=dbName, ssl_ca=SECRET_CA)
                self.cur = self.cnx.cursor()
            except Exception:
                return False
        ## PostgreSQL Connection
        elif dbms == 2:
            try:
                self.cnx = psycopg2.connect(host=ipAddress, port=dbPort, user=dbUsername, password=dbPassword, database=dbName)
                self.cur = self.cnx.cursor()
            except Exception:
                return False
        ## MongoDB Connection
        elif dbms == 3:
            try:
                mongo_uri = 'mongodb://%s:%s@%s:%s/%s' % (dbUsername, dbPassword, ipAddress, dbPort, dbName)
                self.cnx = MongoClient(mongo_uri)
                self.cur = self.cnx[dbName]
            except Exception:
                return False
        ## Oracle DB Connection
        else:
            try:
                dsn = cx_Oracle.makedsn(host=ipAddress,port=dbPort,service_name=dbName)
                self.cnx = cx_Oracle.connect(user=dbUsername, password=dbPassword, dsn= dsn)
                self.cur = self.cnx.cursor()
            except Exception:
                return False
        return self.cnx
    def getCursor(self):
        return self.cur

    def closeConnection(self):
        self.cur.close()
        self.cnx.close()
    def byteArraytoString(self,data):
        output = []
        for elem in data:
            if type(elem) == bytearray:
                elem = elem.decode('utf-8')
            output.append(elem)
        data = tuple(output)
        return data
    def loadDBToElastic(self, statement, customName):
        '''
        Return indexName
        '''
        self.userID = self.getUserID()
        #To access data from MySQL, PostgreSQL, Oracle
        indexName = str(self.userID) + '-' + str(self.reqID)
        if self.dbms != 3:
            generator_df = pd.read_sql(sql=statement, con=self.cnx, chunksize=10000) #chunk load
            for dataframe in generator_df:
                document = dataframe.to_dict(orient='records')
                print(i)
                i+=1
                helpers.bulk(es, document, index=indexName, doc_type=docType)
        #To access data from MongoDB
        else:
            cursor = self.cur.find(statement)
            df =  pd.DataFrame(list(cursor))
            document = df.to_dict(orient='records')
            helpers.bulk(es, document, index=indexName )
        cnx = mysql.connector.connect(**_config)
        cur = cnx.cursor()
        cur.execute("INSERT INTO el.elasticindex (indexName, alias, userID) VALUES (%s, %s, %s) ", (indexName, customName, self.userID))
        cnx.commit()
        cur.close()
        cnx.close()
        return indexName

    def loadFileToElastic(self, filename, customName):
        '''
        Support only csv and json
        Return indexName
        '''
        self.userID = self.getUserID()
        filename, file_extension = os.path.splitext(filename)
        filename = 'data-' + str(self.reqID) + file_extension
        filepath = os.path.join(UPLOAD_FOLDER, filename)
        filename, file_extension = os.path.splitext(filepath)
        indexName = str(self.userID) + '-' + str(self.reqID)
        f = open(filepath,'r')
        if file_extension == '.่json':
            for chunk in pd.read_json(f,lines=True,chunksize=10000):
                document = chunk.to_dict(orient='records')
                helpers.bulk(es, document, index=indexName , doc_type=docType)
        elif file_extension == '.csv':
            for chunk in pd.read_csv(f,chunksize=10000):
                document = chunk.to_dict(orient='records')
                helpers.bulk(es, document, index=indexName , doc_type=docType)
        cnx = mysql.connector.connect(**_config)
        cur = cnx.cursor()
        cur.execute("INSERT INTO el.elasticindex (indexName, alias, userID) VALUES (%s, %s, %s) ", (indexName, customName, self.userID))
        cnx.commit()
        cur.close()
        cnx.close()
        return indexName

    def readFile(self,filename):
        filename, file_extension = os.path.splitext(filename)
        filename = 'data-' + str(self.reqID) + file_extension
        filepath = os.path.join(UPLOAD_FOLDER, filename)
        f = open(filepath,'r')
        return f
        
    def getReadPath(self,filename):
        filename, file_extension = os.path.splitext(filename)
        filename = 'data-' + str(self.reqID) + file_extension
        filepath = os.path.join(UPLOAD_FOLDER, filename)
        return filepath
        
    def writeFile(self,filename,context):
        '''
        filename = filename + file_extension
        open in mode a
        '''
        context = str(context)
        f=open(os.path.join(OUTPUT_FOLDER, filename),'a')
        f.write(context)
        f.close()

    def getWritePath(self,filename):
        return os.path.join(OUTPUT_FOLDER, filename)
    def addEndpoint(self,filename):
         #Put file location to request table
        relPath = os.path.join(OUTPUT_FOLDER, filename)
        if  self.outputList.count(filename) == 0:
            self.outputList.append(filename)
            cnx = mysql.connector.connect(**_config)
            cur = cnx.cursor()
            cur.execute("SELECT result FROM el.request WHERE requestID = %s", (self.reqID,))
            result = cur.fetchone()
            if result[0] is None:
                cur.execute("UPDATE el.request SET result = JSON_ARRAY(JSON_OBJECT('filename', %s, 'path', %s)) where requestID = %s",(filename, relPath, self.reqID))
            else:
                cur.execute("UPDATE el.request SET result = JSON_ARRAY_APPEND(result, '$', JSON_OBJECT('filename', %s, 'path', %s)) where requestID = %s",(filename, relPath, self.reqID))
            cnx.commit()
            cur.close()
            cnx.close()
    def getElasticsearch(self):
        _es = None
        _es = Elasticsearch([{'host': elasticsearchurl, 'port': elasticsearchport}])
        return _es

    def getSparkContext(self,appname):
        sc = SparkContext.getOrCreate()
        return sc

    def getDocType(self):
        return 'doc'
    
es = OSCCP(0).getElasticsearch()
result = []