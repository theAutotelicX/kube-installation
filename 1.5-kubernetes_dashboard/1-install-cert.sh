mkdir certs
cd certs/
openssl genrsa -des3 -passout pass:yourpassword -out dashboard.pass.key 2048
openssl rsa -passin pass:yourpassword -in dashboard.pass.key -out dashboard.key
rm dashboard.pass.key
openssl req -new -key dashboard.key -out dashboard.csr