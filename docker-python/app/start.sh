#!/bin/bash
#move oracle folder
mkdir -p /opt/oracle
mv oracle/ /opt/
sh -c "echo /opt/oracle/instantclient_19_3 > /etc/ld.so.conf.d/oracle-instantclient.conf"
ldconfig
export LD_LIBRARY_PATH=/opt/oracle/instantclient_19_3

cp upload/OSCCP.py /efs/app/upload/OSCCP.py # copy file to efs directory
/spark-2.4.3-bin-hadoop2.7/bin/spark-submit \
--master spark://spark-maste.default.svc.cluster.local:7077 \
--conf spark.driver.host=flask-f.default.svc.cluster.local \
--conf spark.driver.port=8080 \
--conf spark.driver.bindAddress=0.0.0.0 \
--conf spark.executor.cores=1 \
main.py & \
celery -A main.celery worker -l info -O fair