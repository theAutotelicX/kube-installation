Follow this link
https://wiki.onap.org/display/DW/5.+Install+and+Use+Kubernetes+UI

If there is a problem in creating certificate(which it should have) follow:
https://github.com/kubernetes/dashboard/wiki/Certificate-management
https://bugzilla.redhat.com/show_bug.cgi?id=1467669

Then, it should worked:)

Bearer_token can be replace with random token or just use one of the token in kubernetes service

Access kubernetes dashboard with https://<external ip>:<node port>

or create sample user
https://github.com/kubernetes/dashboard/wiki/Creating-sample-user