#!/usr/bin/env python3
from flask import Flask,request,jsonify,send_from_directory, current_app,make_response
import json
import mysql.connector 
import os
from subprocess import Popen,PIPE
import subprocess
from flask_restplus import Api,Resource,fields,reqparse
from flask_cors import CORS,cross_origin
from celery import Celery
from celery.result import AsyncResult
from celery.exceptions import TimeLimitExceeded
import datetime
from datetime import datetime
import time
import jwt
import random
import string
import smtplib
import cx_Oracle
from upload.OSCCP import OSCCP
import random
import math
import psycopg2
import configparser

UPLOAD_FOLDER = '/efs/app/upload/'
OUTPUT_FOLDER = '/efs/app/output/'
SECRET_CA = '/app/secret/ca-cert.pem'
#SECRET_CA = 'D:\\Programming_Projects\\Senior\\Git\\apiConnector\\docker\\app\\secret\\ca-cert.pem'
app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['OUTPUT_FOLDER'] = OUTPUT_FOLDER
app.config['CELERY_BROKER_URL'] = 'amqp://guest@rabbitmq-service:5672//'
app.config['CELERY_RESULT_BACKEND'] = 'amqp://guest@rabbitmq-service:5672//'
app.config['CELERY_IGNORE_RESULT'] = False
app.config['CELERY_TRACK_STARTED'] = True
cors = CORS(app, resources={r"/*": {"origins": "*"}}, supports_credentials=True)
celery = Celery(app.name, backend=app.config['CELERY_RESULT_BACKEND'],broker=app.config['CELERY_BROKER_URL'])
celery.conf.update(app.config)

#--------------------------------------------------------
configINI = configparser.ConfigParser()
configINI.read('config.ini')
config = {
        'user' : configINI['MYSQL']['user'],
        'password': configINI['MYSQL']['password'],
        'host' : configINI['MYSQL']['host'],
        'database' : configINI['MYSQL']['database'],
        'ssl_ca' : SECRET_CA
    }
#86400 = 1day - time limit
@celery.task(time_limit=86400)
def executetask(pathfile,reqid):
    reqid = str(reqid)
    process = Popen(['python3',pathfile,reqid],stdout=PIPE,stderr=PIPE,universal_newlines=True)
    stdout, stderr = process.communicate()
    #wait until process finish
    p_status = process.wait()
    cnx = mysql.connector.connect(**config)
    cur = cnx.cursor()
    now = datetime.now()
    finishedDate = now.strftime("%Y/%m/%d %H:%M:%S")
    osccp = OSCCP(reqid)
    #if no error then return code is 0
    if process.returncode == 0:
        cur.execute("UPDATE el.request SET status = 0, finishedDate = %s WHERE requestID = %s", (finishedDate, reqid))
        cnx.commit()
        cur.close()
        cnx.close()
        filename = 'printout-' + reqid +'.txt'
        osccp.writeFile(filename,stdout)
        osccp.addEndpoint(filename)
        return stdout
    else:
        print(stderr)
        cur.execute("UPDATE el.request SET status = 2, finishedDate = %s WHERE requestID = %s", (finishedDate, reqid))
        cnx.commit()
        cur.close()
        cnx.close()   
        filename = 'error-' + reqid +'.txt'
        osccp.writeFile(filename,stderr)
        osccp.addEndpoint(filename)
        return 'Error executing '+ stderr


#-----------------------------------------------------------

@cross_origin(origin='*')
@app.route('/')
def getNumReq():
    if request.method == 'GET':
        token = request.headers.get('Authorization')
        if isTokenValid(token):
            userid = request.args.get('userid')
            try:
                cnx = mysql.connector.connect(**config)
                cur = cnx.cursor()
            except (mysql.connector.Error) as err:
                return jsonify({'status':'500', 'error': err.msg})
            cur.execute("SELECT status, COUNT(*) AS 'COUNT' FROM el.request WHERE userID = %s GROUP BY status", (userid,))
            fields=[i[0] for i in cur.description]
            rv = cur.fetchall()
            content=[]
            for result in rv:
                finalResult = byteArraytoString(result)
                content.append(dict(zip(fields,finalResult)))
            cur.close()
            cnx.close()
            return jsonify({'content' : content, 'status' : '200'})
        else:
            return jsonify({'status' : '401'})
api = Api(app,doc='/api/')

@cross_origin(origin='*')
@api.route('/users')
class manageUsers(Resource):
    def get(self):
        token = request.headers.get('Authorization')
        try:
            cnx = mysql.connector.connect(**config)
            cur = cnx.cursor()
        except (mysql.connector.Error) as err:
            return jsonify({'status':'500', 'error': err.msg})
        #Check username whether it already exists or not
        if token is None:
            username = request.args.get('username')
            cur.execute('SELECT COUNT(*) FROM el.user WHERE username = %s', (username,))
            result = cur.fetchone()
            cur.close()
            cnx.close()
            if result[0] > 0:
                return jsonify({'isUsed' : 'true'})
            else:
                return jsonify({'isUsed' : 'false'})
        if isTokenValid(token):
            #Get all information for user ID =  'userid'
            userid = request.args.get('userid')
            cur.execute("SELECT username, firstName, lastName, email FROM el.user WHERE userID = %s", (userid,))
            result = cur.fetchone()
            content = {
                'username' : result[0],
                'firstName' : result[1],
                'lastName' : result[2],
                'email' : result[3],
                'userid' : userid
            }
            return jsonify(content)
    def post(self):
        #Get token from request header
        try:
            cnx = mysql.connector.connect(**config)
            cur = cnx.cursor()
        except (mysql.connector.Error) as err:
            return jsonify({'status':'500', 'error': err.msg})
        #Check username whether it already exists or not
        username = request.form.get('username')
        firstName = request.form.get('firstname')
        lastName = request.form.get('lastname')
        email = request.form.get('email')
        password = request.form.get('password')
        cur.execute("INSERT INTO el.user (username, password, firstName, lastName, email) VALUES (%s, %s, %s, %s, %s)", (username, password, firstName, lastName, email))
        cnx.commit()
        numrow = cur.rowcount
        cur.close()
        cnx.close()
        if(numrow > 0):
            return jsonify({'status' : '200'})
        return jsonify({'status' : '500'})
    def patch(self):
        #Get token from request header
        token = request.headers.get('Authorization')
        #Check username whether it already exists or not
        if isTokenValid(token):
            try:
                cnx = mysql.connector.connect(**config)
                cur = cnx.cursor()
            except (mysql.connector.Error) as err:
                return jsonify({'status':'500', 'error': err.msg})
            userid = request.form.get('userid')
            firstName = request.form.get('firstname')
            lastName = request.form.get('lastname')
            email = request.form.get('email')
            password = request.form.get('password')
            cnx = mysql.connector.connect(**config)
            cur = cnx.cursor()
            if password is '':
                cur.execute("UPDATE el.user SET firstName = %s, lastName = %s, email = %s WHERE userid = %s",(firstName, lastName, email, userid))
            else:
                cur.execute("UPDATE el.user SET firstName = %s, lastName = %s, email = %s, password = %s WHERE userid = %s",(firstName, lastName, email, password, userid))
            cnx.commit()
            return jsonify({'status':'200'})

@cross_origin(origin='*')
@api.route('/requests')
class ManageRequests(Resource):
    def get(self):
        token = request.headers.get('Authorization')
        if isTokenValid(token):
            try:
                cnx = mysql.connector.connect(**config)
                cur = cnx.cursor()
            except (mysql.connector.Error) as err:
                return jsonify({'status':'500', 'error': err.msg})
            #Get all requests from user whose userid = 'userid'
            userid = request.args.get('userid')
            reqid = request.args.get('reqid')
            if reqid is None:
                cur.execute("SELECT requestID, name, submittedDate, finishedDate, status, inputType, source FROM el.request WHERE userID = %s ORDER BY requestID DESC", (userid,))
                fields=[i[0] for i in cur.description]
                rv = cur.fetchall()
                content=[]
                for result in rv:
                    finalResult = byteArraytoString(result)
                    content.append(dict(zip(fields,finalResult)))
                cur.close()
                cnx.close()
                return jsonify(content)
            else:
                cur.execute("SELECT * FROM el.request WHERE requestID = %s", (reqid,))
                fields=[i[0] for i in cur.description]
                rv = cur.fetchall()
                content=[]
                for result in rv:
                    finalResult = byteArraytoString(result)
                    content.append(dict(zip(fields,finalResult)))
                cur.close()
                cnx.close()
                try:
                    outputPath = os.path.join(app.config['OUTPUT_FOLDER'], 'printout-'+str(reqid)+'.txt')
                    outputFile = open(outputPath, 'r')
                    output = ''
                    numLines = 20
                    while numLines > 0:
                        lineOutput = outputFile.readline()
                        if lineOutput is None:
                            break
                        output += lineOutput
                        numLines = numLines - 1
                    print(output)
                    content[0]['outputConsole'] = output
                    print(content)
                except Exception as e:
                    print('not use print!')
            return jsonify(content)
    def post(self):
        token = request.headers.get('Authorization')
        if isTokenValid(token):
            try:
                cnx = mysql.connector.connect(**config)
                cur = cnx.cursor()
            except (mysql.connector.Error) as err:
                return jsonify({'status':'500', 'error': err.msg})
            name = request.form.get('name')
            accessID = request.form.get('accessID')
            userID = request.form.get('userid')
            inputType = request.form.get('datasource')
            source = request.form.get('source')
            #Insert New Access if it doesn't exist yet
            if inputType == '0' and not accessID:
                accessName = request.form.get('accessName')
                database = request.form.get('database')
                ip = request.form.get('dbIp')
                port = request.form.get('dbPort')
                user = request.form.get('dbUser')
                pwd = request.form.get('dbPassword')
                dbName = request.form.get('dbName')
                accessID = putAccess(accessName, database, ip, port, user, pwd, dbName, userID)
            now = datetime.now()
            submittedDate = now.strftime("%Y/%m/%d %H:%M:%S")
            if inputType == '0':
                cur.execute("INSERT INTO el.request (name, submittedDate, inputType, source, accessID, userID) VALUES (%s, %s, %s, %s, %s, %s)",(name, submittedDate, inputType, source, accessID, userID))
            else:
                cur.execute("INSERT INTO el.request (name, submittedDate, inputType, source, userID) VALUES (%s, %s, %s, %s, %s)",(name, submittedDate, inputType, source, userID))
            cnx.commit()
            cur.execute("SELECT LAST_INSERT_ID()")
            reqID = cur.fetchone()[0]
            cur.close()
            cnx.close()
            #Save task file into the system
            if 'taskFile' not in request.files:
                return jsonify({'status' : '500'})
            taskfile = request.files['taskFile']
            taskfileExt = os.path.splitext(taskfile.filename)[1]
            filename = 'task-' + str(reqID) + taskfileExt
            taskfile.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            absFilePy = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            #Save data file into the system
            if inputType == '1':
                if 'dataFile' not in request.files:
                    return jsonify({'status' : '500'})
                datafile = request.files['dataFile']
                datafileExt = os.path.splitext(datafile.filename)[1]
                filename = 'data-' + str(reqID) + datafileExt
                datafile.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            #execute task
            executetask.delay(absFilePy,reqID)

            return jsonify({'status' : '200'})
        else:
            return jsonify({'status' : '401'})


def putAccess(accessName, database, ip, port, user, pwd, dbName, userID):
    try:
        cnx = mysql.connector.connect(**config)
        cur = cnx.cursor()
    except (mysql.connector.Error) as err:
        return jsonify({'status':'500', 'error': err.msg})
    cur.execute("INSERT INTO el.access (accessName, dbms, ipAddress, port, dbUsername, dbPassword, dbName, userID) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)",(accessName, database, ip, port, user, pwd, dbName, userID))
    cnx.commit()
    cur.execute("SELECT LAST_INSERT_ID()")
    accessID = cur.fetchone()[0]
    cur.close()
    cnx.close()
    return accessID

@cross_origin(origin='*')
@api.route('/accesses')
class manageAccesses(Resource):
    def get(self):
        token = request.headers.get('Authorization')
        if isTokenValid(token):
            userid = request.args.get('userid')
            try:
                cnx = mysql.connector.connect(**config)
                cur = cnx.cursor()
            except (mysql.connector.Error) as err:
                print("Error in MySQL connector:", err)
                print('Type of error is ' + str(type(err)))
                return jsonify({'status':'500', 'error': err.msg})
            cur.execute("SELECT accessID, accessName, dbms, ipAddress, dbUsername, dbName FROM el.access WHERE userID = %s", (userid,))
            fields=[i[0] for i in cur.description]
            rv = cur.fetchall()
            content=[]
            for result in rv:
                #content.append(dict(zip(fields,result)))
                finalResult = byteArraytoString(result)
                content.append(dict(zip(fields,finalResult)))
            cur.close()
            cnx.close()
            return jsonify(content)
        else:
            return jsonify({'status':'401'})
    def post(self):
            token = request.headers.get('Authorization')
            if isTokenValid(token):
                userID = request.form.get('userid')
                accessName = request.form.get('accessName')
                database = request.form.get('database')
                ip = request.form.get('dbIp')
                port = request.form.get('dbPort')
                user = request.form.get('dbUser')
                pwd = request.form.get('dbPassword')
                dbName = request.form.get('dbName')
                putAccess(accessName, database, ip, port, user, pwd, dbName, userID)
                return jsonify({'status':'200'})
    def delete(self):
        token = request.headers.get('Authorization')
        if isTokenValid(token):
            accessID = request.args.get('accessid')
            try:
                cnx = mysql.connector.connect(**config)
                cur = cnx.cursor()
            except (mysql.connector.Error) as err:
                return jsonify({'status':'500', 'error': err.msg})
            cur.execute("DELETE FROM el.access WHERE accessID = %s", (accessID,))
            cnx.commit()
            cur.close()
            cnx.close()
            return jsonify({'status' : '200'})
        else:
            return jsonify({'status':'401'})

def byteArraytoString(data):
    output = []
    for elem in data:
        if type(elem) == bytearray:
            elem = elem.decode('utf-8')
        output.append(elem)
    data = tuple(output)
    return data

@cross_origin(origin='*')
@api.route('/indices')
class manageIndices(Resource):
    def get(self):
        token = request.headers.get('Authorization')
        if isTokenValid(token):
            userid = request.args.get('userid')
            try:
                cnx = mysql.connector.connect(**config)
                cur = cnx.cursor()
            except (mysql.connector.Error) as err:
                return jsonify({'status':'500', 'error': err.msg})
            cur.execute("SELECT indexID, indexName, alias FROM el.elasticindex WHERE userID = %s", (userid,))
            fields=[i[0] for i in cur.description]
            rv = cur.fetchall()
            content=[]
            for result in rv:
                finalResult = byteArraytoString(result)
                content.append(dict(zip(fields,finalResult)))
            cur.close()
            cnx.close()
            return jsonify(content)
        else:
            return jsonify({'status':'401'})
    def delete(self):
        es = OSCCP(0).getElasticsearch()
        token = request.headers.get('Authorization')
        if isTokenValid(token):
            indexID = request.args.get('indexid')
            try:
                cnx = mysql.connector.connect(**config)
                cur = cnx.cursor()
                cur.execute("SELECT indexName FROM el.elasticindex WHERE indexID = %s", (indexID,))
                indexName = cur.fetchone()
                indexName = byteArraytoString(indexName)
                indexName = indexName[0]
                time.sleep(1)
            except (mysql.connector.Error) as err:
                return jsonify({'status':'500', 'error': err.msg})
            cur.execute("DELETE FROM el.elasticindex WHERE indexID = %s", (indexID,))
            cnx.commit()
            cur.close()
            cnx.close()
            es.indices.delete(index=indexName)
            return jsonify({'status' : '200'})
        else:
            return jsonify({'status':'401'})

@cross_origin(origin='*')
@api.route('/outputs')
class manageOutputs(Resource):
    def get(self):#warning unsafe download
        token = request.headers.get('token')
        userID = request.headers.get('userID')
        reqID = request.headers.get('reqID')
        filename = request.args.get('filename')
        if isTokenValid(token):
            try:
                cnx = mysql.connector.connect(**config)
                cur = cnx.cursor()
            except (mysql.connector.Error) as err:
                return jsonify({'status':'500', 'error': err.msg})
            cur.execute("SELECT result FROM el.request WHERE requestID = %s and userID = %s", (reqID, userID))
            res = cur.fetchone()
            res = byteArraytoString(res)

            print('Prepare sending file...')
            #outFolder = os.path.join(app.config['OUTPUT_FOLDER'], str(reqID)) # use for reqID
            result = send_from_directory(app.config['OUTPUT_FOLDER'] , filename=filename, as_attachment=True)       
        else:
            #return jsonify({'status':'401'})
            #outFolder = os.path.join(app.config['OUTPUT_FOLDER'], str(reqID))
            result = send_from_directory(app.config['OUTPUT_FOLDER'] , filename=filename, as_attachment=True) 
        print(result)
        return result 
@api.route('/testconn')
class testConnection(Resource):
    def get(self):
        accessid = request.args.get('accessid')
        try :
            cnx = mysql.connector.connect(**config)
            cur = cnx.cursor()
        except (mysql.connector.Error) as err:
            return jsonify({'status':'500', 'error': err.msg})
        cur.execute("SELECT dbms, ipAddress, port, dbUsername, dbPassword, dbName FROM el.access WHERE accessID = %s", (accessid,))
        res = cur.fetchone()
        res = byteArraytoString(res)
        dbms = int(res[0])
        ipAddress = res[1]
        dbPort = res[2]
        dbUsername = res[3]
        dbPassword = res[4]
        dbName = res[5]
        cur.close()
        cnx.close()
        #Test connection with MySQL
        if dbms == 1:
            try:
                cnx = mysql.connector.connect(host=ipAddress, port=dbPort, user=dbUsername, password=dbPassword, database=dbName)
                return jsonify({'status':'200'})
            except:
                return jsonify({'status':'500'})
        #Test connection with PostgreSQL
        elif dbms == 2:
            try:
                cnx = psycopg2.connect(host=ipAddress, port=dbPort, user=dbUsername, password=dbPassword, database=dbName, sslmode = 'require')
                return jsonify({'status':'200'})
            except:
                return jsonify({'status':'500'})
        #Test connection with MongoDB
        elif dbms == 3:
            try:
                mongo_uri = 'mongodb://%s:%s@%s:%s/%s' % (dbUsername, dbPassword, ipAddress, dbPort, dbName)
                cnx = MongoClient(mongo_uri)
                return jsonify({'status':'200'})
            except:
                return jsonify({'status':'500'})
        #Test connection with Oracle
        else:
            dsn = cx_Oracle.makedsn(host=ipAddress, port=dbPort, service_name=dbName)
            cnx = cx_Oracle.connect(user=dbUsername, password=dbPassword, dsn=dsn)
        return jsonify({'status':'500'})
@api.route('/login')
class login(Resource):
    def post(self):
        content = {
        'authenticated': 'false',
        'userID': None,
        'firstName': None,
        'token': None,
        }
        user = request.form.get('username')
        pwd = request.form.get('password')
        try:
            cnx = mysql.connector.connect(**config)
            cur = cnx.cursor()
        except (mysql.connector.Error) as err:
            return jsonify({'status':'500', 'error': err.msg})
        cur.execute("SELECT userID, firstName, permission FROM el.user WHERE username = %s AND password = %s",(user,pwd))
        userData = cur.fetchone()
        if(userData):
            content['token'] = jwt.encode({'userID': userData[0], 'permission': userData[2]}, 'OSCCP', algorithm='HS256').decode('UTF-8')
            content['userID'] = userData[0]
            content['firstName'] = userData[1]
            content['authenticated'] =  'true'
            return jsonify(content)
        else:
            return jsonify({'authenticated':'false'})

def randomString(stringLength=10):
    """Generate a random string of fixed length """
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))

@api.route("/forget")
class forgetPwd(Resource):
    def get(self):
        username = request.args.get('username')
        email = request.args.get('email')
        newPassword = randomString(6)
        try:
            cnx = mysql.connector.connect(**config)
            cur = cnx.cursor()
        except (mysql.connector.Error) as err:
            return jsonify({'status':'500', 'error': err.msg})
        cur.execute("SELECT COUNT(*) FROM user WHERE username = %s and email = %s", (username, email))
        if cur.fetchone()[0] == 0:
            return jsonify({'isSuccessful' : 'false', 'status' : '401'})
        cur.execute("UPDATE el.user SET password = %s WHERE username = %s",(newPassword, username))
        cnx.commit()
        #Send Email to user with new reset password
        mailAddress = configINI['RecoveryEmailHost']['email']
        mailTo = email
        mailSubject = '[OSCCP System] Change Password'
        mailBody = 'Subject:'+ mailSubject+'\n'+'\n\nYour reset password is '+newPassword +'\n\n This is an auto message. Do not Reply. '
        mailServer = smtplib.SMTP('smtp.office365.com',587)
        mailServer.starttls()
        mailServer.login(mailAddress,configINI['RecoveryEmailHost']['pass'])
        mailServer.sendmail(mailAddress,mailTo,mailBody)
        mailServer.quit()
        cur.close()
        cnx.close()
        return jsonify({'isSuccessful' : 'true', 'status' : '200'})
def isTokenValid(strtoken):
    try:
        token = strtoken.split(" ")
        jwt.decode(token[1], 'OSCCP', algorithms=['HS256'])
        return True
    except:
        return False

if __name__ == "__main__":
    time.sleep(5)
    app.run(host='0.0.0.0',debug=True,port=5000)
    #app.run(host='127.0.0.1',debug=True,port=5000)