systemctl stop firewalld
systemctl disable firewalld

swapoff -a
sed -i.bak -r 's/(.+ swap .+)/#\1/' /etc/fstab

# Set SELinux in permissive mode (effectively disabling it)
setenforce 0
sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config

cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
exclude=kube*
EOF
#install docker ce
yum install -y yum-utils device-mapper-persistent-data lvm2
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
yum install -y docker-ce

yum install -y kubelet kubeadm kubectl --disableexcludes=kubernetes

systemctl enable docker && systemctl start docker
systemctl enable kubelet && systemctl start kubelet
cat <<EOF > /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
 
sysctl --system

#install cni-network
systemctl enable --now kubelet

modprobe br_netfilter
echo '1' > /proc/sys/net/bridge/bridge-nf-call-iptables

##put kubeadm join here for example
#kubeadm join 192.168.40.19:6443 --token gw0qxo.4xt3sq29dwvxrng6 \
#    --discovery-token-ca-cert-hash sha256:1c147d72f40378bea34f3f587f0a76913697aa7e869026ea99da07e648e4ec96